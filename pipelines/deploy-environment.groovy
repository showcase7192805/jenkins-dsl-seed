pipeline {
    agent any
    parameters {
        choice(choices:['none', 'showcase', 'production'], description: 'Environment to run deployment on', name: 'TARGET')
    }
    options {
      ansiColor('xterm')
    }
    stages {
      stage('Start deployment') {
        when {
          not {
            expression { env.TARGET == 'none' || !env.TARGET }
          }
        }
        stages {
          stage('Clean workspace') {
            steps {
              cleanWs()
            }
          }
          stage('Get Ansible playbooks') {
              steps {
                dir ("ansible-playbooks") {
                  git url: 'https://gitlab.com/showcase7192805/ansible-playbooks.git', branch: 'main'
                }
              }
          }
          stage('Get environment nodes') {
              steps {
                dir ("environments") {
                  git url: 'https://gitlab.com/showcase7192805/environments', branch: env.TARGET
                }
              }
          }
          stage('Run Puppet on environment') {
            steps {
              ansiblePlaybook become: true, colorized: true, credentialsId: 'agent-vms', disableHostKeyChecking: true, installation: 'Ansible-default', inventory: 'environments/inventory.yaml', playbook: 'ansible-playbooks/run_puppet/main.yaml', extras: '-e ansible_become_pass=eryk'
            }
          }
        }
      }
    }
}

