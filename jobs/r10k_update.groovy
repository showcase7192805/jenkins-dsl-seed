multibranchPipelineJob('R10k update environment') {
    branchSources {
        git {
            id('r10k-update') // IMPORTANT: use a constant and unique identifier
            remote('https://gitlab.com/showcase7192805/puppet-control-repo.git')
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(20)
        }
    }
}
