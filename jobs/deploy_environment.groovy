pipelineJob('Deploy environment') {
    definition {
        cps {
            script(readFileFromWorkspace('./pipelines/deploy-environment.groovy'))
            sandbox()
        }
    }
}
